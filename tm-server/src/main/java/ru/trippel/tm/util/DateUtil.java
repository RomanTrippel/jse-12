package ru.trippel.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@NoArgsConstructor
public final class DateUtil {

    @NotNull
    private final static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    @NotNull
    public static Date parseDate(@NotNull final String date) throws ParseException {
        return simpleDateFormat.parse(date);
    }

    @NotNull
    public static String dateFormat(@NotNull final Date date) {
        return simpleDateFormat.format(date);
    }

}