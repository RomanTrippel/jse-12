package ru.trippel.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@NoArgsConstructor
public final class PropertyUtil {

    @NotNull
    private static Properties loadProperty() throws IOException {
        @NotNull final Properties properties = new Properties();
        @NotNull final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        @Nullable final InputStream is = classLoader.getResourceAsStream("application.properties");
        properties.load(is);
        return properties;
    }

    @NotNull
    public static String dbDriver() throws IOException {
        return loadProperty().getProperty("db.driver");
    }

    @NotNull
    public static String dbHost() throws IOException {
        return loadProperty().getProperty("db.host");
    }

    @NotNull
    public static String dbLogin() throws IOException {
        return loadProperty().getProperty("db.login");
    }

    @NotNull
    public static String dbPassword() throws IOException {
        return loadProperty().getProperty("db.password");
    }

}
