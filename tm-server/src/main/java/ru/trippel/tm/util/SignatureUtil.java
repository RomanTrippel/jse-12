package ru.trippel.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class SignatureUtil {

    @Nullable
    public static String sign(@Nullable final Object value) {
        try {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writeValueAsString(value);
            return sign(json);
        } catch (final JsonProcessingException e) {
            return null;
        }
    }

    @Nullable
    public static String sign(@Nullable final String value) {
        if (value == null) return null;
        @NotNull final String salt = "~какаятосоль*";
        @NotNull final int cycle = 10;
        @Nullable String result = value;
        for (int i = 0; i < cycle; i++) {
            result = PasswordHashUtil.getHash(salt + result + salt);
        }
        return result;
    }

}