package ru.trippel.tm.util;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import java.sql.Connection;
import java.sql.DriverManager;

@NoArgsConstructor
public final class ConnectionUtil {

    @Nullable
    public static Connection getConnection() {
        @Nullable Connection result = null;
        try {
            @NonNull final String dbDriver = PropertyUtil.dbDriver();
            @NonNull final String dbHost = PropertyUtil.dbHost();
            @NonNull final String dbLogin = PropertyUtil.dbLogin();
            @NonNull final String dbPassword = PropertyUtil.dbPassword();
            Class.forName(dbDriver).newInstance();
            result = DriverManager.getConnection(dbHost, dbLogin, dbPassword);
            result.setAutoCommit(false);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
