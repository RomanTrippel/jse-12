package ru.trippel.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.entity.ISortable;
import ru.trippel.tm.enumeration.Status;

import java.util.Date;
import java.util.Objects;

@Setter
@Getter
@NoArgsConstructor
public final class Task extends AbstractEntity implements ISortable {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date dateStart = new Date();

    @NotNull
    private Date dateFinish = new Date();

    @NotNull
    private String projectId = "";

    @NotNull
    private String userId = "";

    @NotNull
    private Status status = Status.PLANNED;

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        @NotNull final Task task = (Task) o;
        return Objects.equals(id, task.id) &&
                Objects.equals(projectId, task.projectId) &&
                Objects.equals(name, task.name) &&
                Objects.equals(description, task.description) &&
                Objects.equals(dateStart, task.dateStart) &&
                Objects.equals(dateFinish, task.dateFinish);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, projectId, name, description, dateStart, dateFinish);
    }

}