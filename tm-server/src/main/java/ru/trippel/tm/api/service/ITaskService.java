package ru.trippel.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.enumeration.SortingMethod;

import java.sql.SQLException;
import java.util.List;

public interface ITaskService extends IService<Task> {

    @NotNull
    List<Task> findAll() throws SQLException;

    @Nullable
    List<Task> findAll(@Nullable String userId) throws SQLException;

    @Nullable
    List<Task> findAll(@Nullable String userId, @NotNull SortingMethod sortingMethod) throws SQLException;

    @Nullable
    Task findOne(@Nullable String id) throws SQLException;

    @Nullable
    List<Task> findByPart(@Nullable String userId, @NotNull String searchText) throws SQLException;

    @Nullable
    Task persist(@Nullable Task task) throws SQLException;

    @Nullable
    Task merge(@Nullable Task task) throws SQLException;

    void remove(@Nullable String id) throws SQLException;

    void removeAllByProjectId(@NotNull String projectId) throws SQLException;

    void clear(@Nullable String userId) throws SQLException;

    void persist(@NotNull List<Task> taskList) throws SQLException;

}
