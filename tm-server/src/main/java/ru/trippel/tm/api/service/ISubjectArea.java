package ru.trippel.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.context.IServiceLocator;

import java.sql.SQLException;

public interface ISubjectArea {

    void write(@NotNull IServiceLocator serviceLocator) throws SQLException;

    void read(@NotNull IServiceLocator serviceLocator) throws SQLException;

}
