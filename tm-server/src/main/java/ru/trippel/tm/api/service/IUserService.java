package ru.trippel.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    List<User> findAll() throws SQLException;

    @Nullable
    User findOne(@Nullable String id) throws SQLException;

    @Nullable
    User findByLoginName(@Nullable String loginName) throws SQLException;

    @Nullable
    User persist(@Nullable User user) throws SQLException;

    @Nullable
    User merge(@Nullable User user) throws SQLException;

    void remove(@Nullable String id) throws SQLException;

    boolean checkLogin(@Nullable final String name) throws SQLException;

}
