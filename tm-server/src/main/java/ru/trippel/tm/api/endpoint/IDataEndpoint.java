package ru.trippel.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Session;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.io.IOException;

@WebService
public interface IDataEndpoint {

    void dataSerializationSave(
            @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    void dataFasterxmlXmlSave(
            @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    void dataFasterxmlJsonSave(
            @WebParam(name = "session") @Nullable Session session
    ) throws IOException, Exception;

    void dataJaxbXmlSave(
            @WebParam(name = "session") @Nullable Session session
    ) throws JAXBException, Exception;

    void dataJaxbJsonSave(
            @WebParam(name = "session") @Nullable Session session
    ) throws JAXBException, Exception;

    void dataSerializationLoad(
            @WebParam(name = "session") @Nullable Session session
    ) throws IOException, ClassNotFoundException, Exception;

    void dataFasterxmlXmlLoad(
            @WebParam(name = "session") @Nullable Session session
    ) throws IOException, Exception;

    void dataFasterxmlJsonLoad(
            @WebParam(name = "session") @Nullable Session session
    ) throws IOException, Exception;

    void dataJaxbXmlLoad(
            @WebParam(name = "session") @Nullable Session session
    ) throws JAXBException, Exception;

    void dataJaxbJsonLoad(
            @WebParam(name = "session") @Nullable Session session
    ) throws JAXBException, Exception;

}
