package ru.trippel.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Session;
import ru.trippel.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @Nullable
    @WebMethod
    User createUser(
            @WebParam(name = "loginName") @NotNull String loginName,
            @WebParam(name = "password") @NotNull String password
    ) throws Exception;

    @WebMethod
    void removeAllUsers(
            @WebParam(name = "session") @NotNull Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    List<User> findAllUser(
            @WebParam(name = "session") @NotNull Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    User findOneUser(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "userId") @NotNull String userId
    ) throws Exception;

    @Nullable
    @WebMethod
    User findByLoginNameUser(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "loginName") @NotNull final String loginName
    ) throws Exception;

    @Nullable
    @WebMethod
    User updateUser(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "user") @NotNull User user
    ) throws Exception;

    @WebMethod
    void removeUser(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "userId") @NotNull String userId
    ) throws Exception;

    @WebMethod
    boolean checkLoginUser(
            @WebParam(name = "loginName") @NotNull String loginName
    ) throws Exception;

}
