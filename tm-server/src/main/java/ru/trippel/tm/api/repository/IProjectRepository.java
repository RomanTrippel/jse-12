package ru.trippel.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Project;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    List<Project> findAll() throws SQLException;

    @Nullable
    List<Project> findAll(@NotNull String userId) throws SQLException;

    @Nullable
    List<Project> findAll(@NotNull String userId, @NotNull Comparator<Project> comparator) throws SQLException;

    @Nullable
    Project findOne(@NotNull String id) throws SQLException;

    @Nullable
    List<Project> findByPart(@NotNull String userId, @NotNull String searchText) throws SQLException;

    @Nullable
    Project persist(@NotNull Project project) throws SQLException;

    @Nullable
    Project merge(@NotNull Project project) throws SQLException;

    void remove(@NotNull String id) throws SQLException;

    void removeAll() throws SQLException;

    void clear(@NotNull String userId) throws SQLException;

    void persist(@NotNull List<Project> projectList) throws SQLException;

}
