package ru.trippel.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Session;

public interface ISessionRepository extends IRepository<Session> {

    @Nullable
    Session findOne(@NotNull String sessionId, @NotNull String userId);

    @Nullable
    Session remove(@NotNull String sessionId, @NotNull String userId);

}
