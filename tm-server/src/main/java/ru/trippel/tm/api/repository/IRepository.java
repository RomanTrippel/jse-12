package ru.trippel.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.List;

public interface IRepository<T> {

    @NotNull
    List<T> findAll() throws SQLException;

    @Nullable
    T findOne(@NotNull String id) throws SQLException;

    @Nullable
    T persist(@NotNull T t) throws SQLException;

    @Nullable
    T merge(@NotNull T t) throws SQLException;

    void remove(@NotNull String id) throws SQLException;

    void removeAll() throws SQLException;

}
