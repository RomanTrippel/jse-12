package ru.trippel.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Task;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository <Task> {

    @NotNull
    List<Task> findAll() throws SQLException;

    @Nullable
    List<Task> findAll(@NotNull String userId) throws SQLException;

    @Nullable
    List<Task> findAll(@NotNull String userId, @NotNull Comparator<Task> comparator) throws SQLException;

    @Nullable
    Task findOne(@NotNull String id) throws SQLException;

    @Nullable
    List<Task> findByPart(@NotNull String userId, @NotNull String searchText) throws SQLException;

    @Nullable
    Task persist(@NotNull Task task) throws SQLException;

    @Nullable
    Task merge(@NotNull Task task) throws SQLException;

    void remove(@NotNull String id) throws SQLException;

    void removeAll() throws SQLException;

    void clear(@NotNull String userId) throws SQLException;

    void removeByProjectId(@NotNull String projectId) throws SQLException;

    void persist(@NotNull List<Task> taskList) throws SQLException;

}
