package ru.trippel.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Session;
import ru.trippel.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    Task createTask(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "taskName") @NotNull String taskName
    ) throws Exception;

    @WebMethod
    void clearTasks(
            @WebParam(name = "session") @NotNull Session session
    ) throws Exception;

    @WebMethod
    void removeAllTasks(
            @WebParam(name = "session") @NotNull Session session
    ) throws Exception;

    @WebMethod
    void findAllTask(
            @WebParam(name = "session") @NotNull Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Task findOneTask(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "taskId") @NotNull String taskId
    ) throws Exception;

    @Nullable
    @WebMethod
    Task updateTask(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "task") @NotNull Task task
    ) throws Exception;

    @Nullable
    @WebMethod
    void removeTask(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "taskId") @NotNull String taskId
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findAllTasksBySort(
            @WebParam(name = "session") @NotNull Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findAllTasksByUserId(
            @WebParam(name = "session") @NotNull Session session
    ) throws Exception;

    @WebMethod
    void removeAllTaskByProjectId(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "projectId") @NotNull String projectId
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findTaskByPart(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "searchPhrase") @NotNull String searchText
    ) throws Exception;

}
