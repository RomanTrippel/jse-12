package ru.trippel.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Session;
import ru.trippel.tm.enumeration.TypeRole;

import java.sql.SQLException;

public interface ISessionService extends IService<Session> {

    @Nullable
    void remove(@Nullable Session session) throws SQLException;

    void validate (@Nullable Session session) throws Exception;

    void validate (@Nullable Session session, @NotNull TypeRole role) throws Exception;

}
