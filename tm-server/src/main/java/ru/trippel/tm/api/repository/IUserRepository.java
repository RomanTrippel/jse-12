package ru.trippel.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    List<User> findAll() throws SQLException;

    @Nullable
    User findOne(@NotNull String id) throws SQLException;

    @Nullable
    User findByLoginName(String login) throws SQLException;

    @Nullable
    User persist(@NotNull User user) throws SQLException;

    @Nullable
    User merge(@NotNull User user) throws SQLException;

    void remove(@NotNull String id) throws SQLException;

    void removeAll() throws SQLException;

}
