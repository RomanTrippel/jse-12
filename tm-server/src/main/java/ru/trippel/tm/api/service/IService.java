package ru.trippel.tm.api.service;

import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.List;

public interface IService<T> {

    @Nullable
    List<T> findAll() throws SQLException;

    @Nullable
    T findOne(@Nullable String id) throws SQLException;

    @Nullable
    T persist(@Nullable T t) throws SQLException;

    @Nullable
    T merge(@Nullable T t) throws SQLException;

    void remove(@Nullable String id) throws SQLException;

    void removeAll() throws SQLException;

}
