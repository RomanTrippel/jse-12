package ru.trippel.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.enumeration.SortingMethod;

import java.sql.SQLException;
import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    List<Project> findAll() throws SQLException;

    @Nullable
    List<Project> findAll(@Nullable String userId) throws SQLException;

    @Nullable
    List<Project> findAll(@Nullable String userId, @NotNull SortingMethod sortingMethod) throws SQLException;

    @Nullable
    Project findOne(@Nullable String id) throws SQLException;

    @Nullable
    List<Project> findByPart(@Nullable String userId, @NotNull String searchText) throws SQLException;

    @Nullable
    Project persist(@Nullable Project project) throws SQLException;

    @Nullable
    Project merge(@Nullable Project project) throws SQLException;

    void remove(@Nullable String id) throws SQLException;

    void clear(@Nullable String userId) throws SQLException;

    void persist(@NotNull List<Project> projectService) throws SQLException;

}
