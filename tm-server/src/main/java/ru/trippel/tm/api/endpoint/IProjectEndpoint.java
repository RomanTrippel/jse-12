package ru.trippel.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @Nullable
    @WebMethod
    Project createProject(
            @WebParam(name = "session") @NotNull  Session session,
            @WebParam(name = "projectName") @NotNull String projectName
    ) throws Exception;

    @WebMethod
    void clearProjects(
            @WebParam(name = "session") @NotNull Session session
    ) throws Exception;

    @WebMethod
    void removeAllProjects(
            @WebParam(name = "session") @NotNull Session session
    ) throws Exception;

    @NotNull
    @WebMethod
    List<Project> findAllProject(
            @WebParam(name = "session") @NotNull Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Project findOneProject(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "projectId") @NotNull String projectId
    ) throws Exception;

    @Nullable
    @WebMethod
    Project updateProject(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "project") @NotNull Project project
    ) throws Exception;

    @Nullable
    @WebMethod
    void removeProject(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "projectId") @NotNull String projectId
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Project> findAllProjectsBySort(
            @WebParam(name = "session") @NotNull Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Project> findAllProjectsByUserId(
            @WebParam(name = "session") @NotNull Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Project> findProjectByPart(
            @WebParam(name = "session") @NotNull Session session,
            @WebParam(name = "searchPhrase") @NotNull String searchText
    ) throws Exception;

}
