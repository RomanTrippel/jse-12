package ru.trippel.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.service.ISessionService;
import ru.trippel.tm.entity.Session;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.repository.SessionRepository;
import ru.trippel.tm.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class SessionService implements ISessionService {

    @Nullable
    @Override
    public Session persist(@Nullable final Session session) throws SQLException {
        if (session == null) return null;
        @Nullable Session result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new SessionRepository(connection).persist(session);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Override
    public void remove(@Nullable final Session session) throws SQLException {
        if (session == null) return;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return;
        try {
            new SessionRepository(connection).remove(session.getId());
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) throws SQLException {
        if (id == null || id.isEmpty()) return null;
        @Nullable Session result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new SessionRepository(connection).findOne(id);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }


    @Override
    public void validate(@Nullable final Session session, @NotNull TypeRole role) throws Exception {
        if (!role.equals(session.getRole())) throw new Exception("Not enough rights.");
        validate(session);
    }

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (session == null) throw new Exception("You must log in.");
        if (session.getUserId() == null ||
                session.getSignature() == null ||
                session.getRole() == null) throw new Exception("You must log in.");
        @Nullable final Session sessionInBase = findOne(session.getId());
        if (sessionInBase == null) throw new Exception("No session.");
        @Nullable final String signature = session.getSignature();
        @Nullable final String signatureInBase = sessionInBase.getSignature();
        if (signatureInBase == null) throw new Exception("You must log in.");
        if (!(signatureInBase.equals(signature))) throw new Exception("You must log in.");
        final long passedTime = System.currentTimeMillis() - sessionInBase.getCreateDate();
        if (passedTime > 10*60*1000) {
            remove(session.getId());
            throw new Exception("The session time of 10 minutes has expired. You need to log in.");
        }
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        return null;
    }

    @Nullable
    @Override
    public Session merge(@Nullable final Session session) {
        return null;
    }

    @Override
    public void remove(@Nullable final String id) {
    }

    @Override
    public void removeAll() {
    }

}
