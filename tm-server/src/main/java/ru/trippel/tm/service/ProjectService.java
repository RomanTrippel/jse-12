package ru.trippel.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.service.IProjectService;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.enumeration.SortingMethod;
import ru.trippel.tm.repository.ProjectRepository;
import ru.trippel.tm.util.ComparatorUtil;
import ru.trippel.tm.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public final class ProjectService implements IProjectService {

    @Nullable
    @Override
    public Project persist(@Nullable final Project project) throws SQLException {
        if (project == null) return null;
        @Nullable Project result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new ProjectRepository(connection).persist(project);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
         connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId, @NotNull final SortingMethod sortingMethod)
            throws SQLException {
        if (userId == null) return null;
        if (userId.isEmpty()) return null;
        @Nullable List<Project> result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            @NotNull final Comparator<Project> comparator = ComparatorUtil.getComparator(sortingMethod);
            result = new ProjectRepository(connection).findAll(userId, comparator);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Override
    public void clear(@Nullable final String userId) throws SQLException {
        if (userId == null) return;
        if (userId.isEmpty()) return;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return;
        try {
            new ProjectRepository(connection).clear(userId);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws SQLException {
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return;
        try {
            new ProjectRepository(connection).removeAll();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll() throws SQLException {
        @Nullable List<Project> result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new ProjectRepository(connection).findAll();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) throws SQLException {
        if (id == null || id.isEmpty()) return null;
        @Nullable Project result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new ProjectRepository(connection).findOne(id);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public Project merge(@Nullable final Project project) throws SQLException {
        if (project == null) return null;
        @Nullable Project result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new ProjectRepository(connection).merge(project);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) return null;
        @Nullable List<Project> result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new ProjectRepository(connection).findAll(userId);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public List<Project> findByPart(@Nullable final String userId, @NotNull final String searchText)
            throws SQLException {
        if (userId == null || userId.isEmpty()) return null;
        if (searchText == null || searchText.isEmpty()) return null;
        @Nullable List<Project> result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new ProjectRepository(connection).findByPart(userId, searchText);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Override
    public void remove(@Nullable final String id) throws SQLException {
        if (id == null || id.isEmpty()) return;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return;
        try {
            new ProjectRepository(connection).remove(id);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    public void persist(@NotNull final List<Project> projectList) throws SQLException {
        if (projectList == null || projectList.size() == 0) return;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return;
        try {
            new ProjectRepository(connection).persist(projectList);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

}
