package ru.trippel.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.service.IUserService;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.repository.UserRepository;
import ru.trippel.tm.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class UserService implements IUserService {

    @Nullable
    @Override
    public List<User> findAll() throws SQLException {
        @Nullable List<User> result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new UserRepository(connection).findAll();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public User findOne(@Nullable String id) throws SQLException {
        if (id == null || id.isEmpty()) return null;
        @Nullable User result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new UserRepository(connection).findOne(id);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public User findByLoginName(@Nullable String loginName) throws SQLException {
        if (loginName == null) return null;
        if (loginName.isEmpty()) return null;
        @Nullable User result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new UserRepository(connection).findByLoginName(loginName);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public User persist(@Nullable User user) throws SQLException {
        if (user == null) return null;
        @Nullable User result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new UserRepository(connection).persist(user);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public User merge(@Nullable User user) throws SQLException {
        if (user == null) return null;
        @Nullable User result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new UserRepository(connection).merge(user);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Override
    public void remove(@Nullable String id) throws SQLException {
        if (id == null || id.isEmpty()) return;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return;
        try {
            new UserRepository(connection).remove(id);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws SQLException {
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return;
        try {
            new UserRepository(connection).removeAll();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    public boolean checkLogin(@Nullable final String name) throws SQLException {
        if (name == null) return false;
        if (name.isEmpty()) return false;
        @NotNull final List<User> userList = findAll();
        for (@NotNull final User user: userList) {
            if (user.getLoginName().equals(name)) {
                return true;
            }
        }
        return false;
    }

}
