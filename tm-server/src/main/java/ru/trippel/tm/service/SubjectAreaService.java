package ru.trippel.tm.service;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.service.IProjectService;
import ru.trippel.tm.api.service.ISubjectArea;
import ru.trippel.tm.api.service.ITaskService;
import ru.trippel.tm.api.service.IUserService;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.entity.User;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonRootName("SubjectAreaService")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SubjectAreaService")
public class SubjectAreaService implements ISubjectArea, Serializable {

    private static final long serialVersionUID = 1;

    @Nullable
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    @JacksonXmlProperty(localName = "project")
    @JacksonXmlElementWrapper(localName = "projects")
    private List<Project> projectList = new ArrayList<>();

    @Nullable
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    @JacksonXmlProperty(localName = "task")
    @JacksonXmlElementWrapper(localName = "tasks")
    private List<Task> taskList = new ArrayList<>();

    @Nullable
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    @JacksonXmlProperty(localName = "user")
    @JacksonXmlElementWrapper(localName = "users")
    private List<User> userList = new ArrayList<>();

    public void write(@NotNull final IServiceLocator serviceLocator) throws SQLException {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final IUserService userService = serviceLocator.getUserService();
            serviceLocator.getUserService().removeAll();
        if (userList != null) {
            for (@NonNull final User user: userList) {
                userService.persist(user);
            }
        }
        if (projectList != null) {
            for (@NonNull final Project project: projectList){
                projectService.persist(project);
            }
        }
        if (taskList != null) {
            for (@NonNull final Task task: taskList){
                taskService.persist(task);
            }
        }
    }

    public void read(@NotNull final IServiceLocator serviceLocator) throws SQLException {
        projectList = serviceLocator.getProjectService().findAll();
        taskList = serviceLocator.getTaskService().findAll();
        userList = serviceLocator.getUserService().findAll();
    }

}