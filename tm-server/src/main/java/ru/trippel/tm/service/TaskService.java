package ru.trippel.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.service.ITaskService;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.enumeration.SortingMethod;
import ru.trippel.tm.repository.TaskRepository;
import ru.trippel.tm.util.ComparatorUtil;
import ru.trippel.tm.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    @Nullable
    @Override
    public Task persist(@Nullable Task task) throws SQLException {
        if (task == null) return null;
        @Nullable Task result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new TaskRepository(connection).persist(task);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Override
    public void clear(@Nullable final String userId) throws SQLException {
        if (userId == null) return;
        if (userId.isEmpty()) return;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return;
        try {
            new TaskRepository(connection).clear(userId);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws SQLException {
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return;
        try {
            new TaskRepository(connection).removeAll();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() throws SQLException {
        @Nullable List<Task> result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new TaskRepository(connection).findAll();
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public Task findOne(@Nullable String id) throws SQLException {
        if (id == null || id.isEmpty()) return null;
        @Nullable Task result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new TaskRepository(connection).findOne(id);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public Task merge(@Nullable Task task) throws SQLException {
        if (task == null) return null;
        @Nullable Task result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new TaskRepository(connection).merge(task);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Override
    public void remove(@Nullable String id) throws SQLException {
        if (id == null || id.isEmpty()) return;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return;
        try {
            new TaskRepository(connection).remove(id);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String userId, @NotNull final SortingMethod sortingMethod)
            throws SQLException {
        if (userId == null) return null;
        if (userId.isEmpty()) return null;
        @Nullable List<Task> result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            @NotNull final Comparator<Task> comparator = ComparatorUtil.getComparator(sortingMethod);
            result = new TaskRepository(connection).findAll(userId, comparator);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) return null;
        @Nullable List<Task> result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new TaskRepository(connection).findAll(userId);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Override
    public void removeAllByProjectId(@NotNull final String projectId) throws SQLException {
        if (projectId == null || projectId.isEmpty()) return;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return;
        try {
            new TaskRepository(connection).removeByProjectId(projectId);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public List<Task> findByPart(@Nullable final String userId, @NotNull final String searchText) throws SQLException {
        if (userId == null || userId.isEmpty()) return null;
        if (searchText == null || searchText.isEmpty()) return null;
        @Nullable List<Task> result = null;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return null;
        try {
            result = new TaskRepository(connection).findByPart(userId, searchText);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return result;
    }

    @Override
    public void persist(@NotNull final List<Task> taskList) throws SQLException {
        if (taskList == null || taskList.size() == 0) return;
        final @Nullable Connection connection = ConnectionUtil.getConnection();
        if (connection == null) return;
        try {
            new TaskRepository(connection).persist(taskList);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

}
