package ru.trippel.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.service.IDataService;
import ru.trippel.tm.api.service.ISubjectArea;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.sql.SQLException;

public class DataService  implements IDataService {

    @NotNull
    final IServiceLocator serviceLocator;

    public DataService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void dataSerializationSave() throws IOException, SQLException {
        @NotNull final ISubjectArea subjectAreaService = serviceLocator.getSubjectAreaService();
        subjectAreaService.read(serviceLocator);
        @NotNull final File folder = new File("data");
        if (!folder.exists()) folder.mkdir();
        @NotNull final FileOutputStream fos = new FileOutputStream("data/data.bin");
        @NotNull final ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(subjectAreaService);
        oos.close();
        fos.close();
    }

    @Override
    public void dataFasterxmlXmlSave() throws IOException, SQLException {
        @NotNull final ISubjectArea subjectAreaService = new SubjectAreaService();
        subjectAreaService.read(serviceLocator);
        @NotNull final File folder = new File("data");
        if (!folder.exists()) folder.mkdir();
        @NotNull final File file = new File("data/fasterxml.xml");
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(file, subjectAreaService);
    }

    @Override
    public void dataFasterxmlJsonSave() throws IOException, SQLException {
        @NotNull final ISubjectArea subjectAreaService = new SubjectAreaService();
        subjectAreaService.read(serviceLocator);
        @NotNull final File folder = new File("data");
        if (!folder.exists()) folder.mkdir();
        @NotNull final File file = new File("data/fasterxml.json");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, subjectAreaService);
    }

    @Override
    public void dataJaxbXmlSave() throws JAXBException, SQLException {
        @NotNull final ISubjectArea subjectAreaService = new SubjectAreaService();
        subjectAreaService.read(serviceLocator);
        @NotNull final File folder = new File("data");
        if (!folder.exists()) folder.mkdir();
        @NotNull final File file = new File("data/jaxb.xml");
        @NotNull final JAXBContext context = JAXBContext.newInstance(SubjectAreaService.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(subjectAreaService,file);
    }

    @Override
    public void dataJaxbJsonSave() throws JAXBException, SQLException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final ISubjectArea subjectAreaService = new SubjectAreaService();
        subjectAreaService.read(serviceLocator);
        @NotNull final File folder = new File("data");
        if (!folder.exists()) folder.mkdir();
        @NotNull final File file = new File("data/jaxb.json");
        @NotNull final JAXBContext context = JAXBContext.newInstance(SubjectAreaService.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(subjectAreaService,file);
    }

    @Override
    public void dataSerializationLoad() throws IOException, ClassNotFoundException, SQLException {
        @NotNull final FileInputStream fis = new FileInputStream("data/data.bin");
        @NotNull final ObjectInputStream ois = new ObjectInputStream(fis);
        @NotNull final ISubjectArea subjectAreaService = (ISubjectArea) ois.readObject();
        subjectAreaService.write(serviceLocator);
        ois.close();
        fis.close();
    }

    @Override
    public void dataFasterxmlXmlLoad() throws IOException, SQLException {
        @NotNull final File file = new File("data/fasterxml.xml");
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        @NotNull final ISubjectArea subjectAreaService = xmlMapper.readValue(file, SubjectAreaService.class);
        subjectAreaService.write(serviceLocator);
    }

    @Override
    public void dataFasterxmlJsonLoad() throws IOException, SQLException {
        @NotNull final File file = new File("data/fasterxml.json");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        @NotNull final ISubjectArea subjectAreaService = objectMapper.readValue(file, SubjectAreaService.class);
        subjectAreaService.write(serviceLocator);
    }

    @Override
    public void dataJaxbXmlLoad() throws JAXBException, SQLException {
        @NotNull final File file = new File("data/jaxb.xml");
        @NotNull final JAXBContext context = JAXBContext.newInstance(SubjectAreaService.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final ISubjectArea subjectAreaService = (ISubjectArea) unmarshaller.unmarshal(file);
        subjectAreaService.write(serviceLocator);
    }

    @Override
    public void dataJaxbJsonLoad() throws JAXBException, SQLException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final File file = new File("data/jaxb.json");
        @NotNull final StreamSource streamSource = new StreamSource(file);
        @NotNull final JAXBContext context = JAXBContext.newInstance(SubjectAreaService.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        @NotNull final ISubjectArea subjectAreaService =
                unmarshaller.unmarshal(streamSource, SubjectAreaService.class).getValue();
        subjectAreaService.write(serviceLocator);
    }

}
