package ru.trippel.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.endpoint.*;
import ru.trippel.tm.api.service.*;
import ru.trippel.tm.endpoint.*;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.service.*;
import ru.trippel.tm.util.PasswordHashUtil;

import javax.xml.ws.Endpoint;
import java.sql.SQLException;

@Getter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectService projectService = new ProjectService();

    @NotNull
    private final ITaskService taskService = new TaskService();

    @NotNull
    private final IUserService userService = new UserService();

    @NotNull
    private final ISessionService sessionService = new SessionService();

    @NotNull
    public final ISubjectArea subjectAreaService = new SubjectAreaService();

    @NotNull
    private final IDataService dataService = new DataService(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final IDataEndpoint dataEndpoint = new DataEndpoint(this);

    public void start() {
        try {
            initDefaultUser();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Endpoint.publish("http://localhost:8080/ProjectEndpoint?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/TaskEndpoint?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/UserEndpoint?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/SessionEndpoint?wsdl", sessionEndpoint);
        Endpoint.publish("http://localhost:8080/DataEndpoint?wsdl", dataEndpoint);
        System.out.println("Sm-server started successfully.");
    }

    public void initDefaultUser() throws SQLException {
        @NotNull final User admin = new User();
        admin.setLoginName("admin");
        admin.setPassword(PasswordHashUtil.getHash("admin"));
        admin.setRole(TypeRole.ADMIN);
        userService.persist(admin);
        @NotNull final User user = new User();
        user.setLoginName("user");
        user.setPassword(PasswordHashUtil.getHash("user"));
        userService.persist(user);
    }

}