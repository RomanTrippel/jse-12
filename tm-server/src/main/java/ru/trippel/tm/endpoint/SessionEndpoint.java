package ru.trippel.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.endpoint.ISessionEndpoint;
import ru.trippel.tm.entity.Session;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.util.PasswordHashUtil;
import ru.trippel.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.trippel.tm.api.endpoint.ISessionEndpoint")
public class SessionEndpoint implements ISessionEndpoint {

    private IServiceLocator serviceLocator;

    public SessionEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    @WebMethod
    public Session createSession(
            @WebParam(name = "loginName") final @NotNull String loginName,
            @WebParam(name = "password") final @NotNull String password
    ) throws Exception {
        @Nullable final User user = serviceLocator.getUserService().findByLoginName(loginName);
        if (user == null) throw new Exception("Wrong login.");
        @Nullable final String hash = PasswordHashUtil.getHash(password);
        if (hash == null) throw new Exception("Error calculating hash.");
        if (!hash.equals(user.getPassword())) throw new Exception("Wrong pass");
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        session.setSignature(SignatureUtil.sign(session));
        serviceLocator.getSessionService().persist(session);
        return session;
    }

    @Override
    @WebMethod
    public void removeSession(@WebParam(name = "session") final Session session) throws Exception {
        serviceLocator.getSessionService().remove(session);
    }

}
