package ru.trippel.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.endpoint.ITaskEndpoint;
import ru.trippel.tm.entity.Session;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.SortingMethod;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.trippel.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {

    private IServiceLocator serviceLocator;

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public Task createTask(@WebParam(name = "session") @NotNull final Session session,
                           @WebParam(name = "taskName") @NotNull final String taskName
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setUserId(session.getUserId());
        return serviceLocator.getTaskService().persist(task);
    }

    @Override
    @WebMethod
    public void clearTasks(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllTasks(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeAll();
    }

    @Override
    @WebMethod
    public void findAllTask(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().findAll();
    }

    @Nullable
    @Override
    @WebMethod
    public Task findOneTask(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "taskId") @NotNull final String taskId
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOne(taskId);
    }

    @Nullable
    @Override
    @WebMethod
    public Task updateTask(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "task") @NotNull final Task task
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final Task taskTemp = task;
        taskTemp.setUserId(session.getUserId());
        return serviceLocator.getTaskService().merge(taskTemp);
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "taskId") @NotNull final String taskId
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().remove(taskId);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> findAllTasksBySort(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findOne(userId);
        if (userId == null || user == null) return null;
        @NotNull final SortingMethod sortingMethod = user.getTaskSortingMethod();
        return serviceLocator.getTaskService().findAll(session.getUserId(), sortingMethod);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> findAllTasksByUserId(
            @WebParam(name = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllTaskByProjectId(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "projectId") @NotNull final String projectId
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeAllByProjectId(projectId);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Task> findTaskByPart(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "searchPhrase") @NotNull final String searchText
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByPart(session.getUserId(), searchText);
    }

}
