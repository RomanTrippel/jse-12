package ru.trippel.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.ISessionRepository;
import ru.trippel.tm.entity.Session;
import ru.trippel.tm.enumeration.TypeRole;

import java.sql.*;
import java.util.List;

@NoArgsConstructor
public final class SessionRepository implements ISessionRepository {

    @Nullable
    public Connection connection;

    public SessionRepository(@Nullable final Connection connection) {
        this.connection = connection;
    }

    @Nullable
    @Override
    public Session persist(@NotNull final Session session) throws SQLException {
        @NotNull final String query = "INSERT INTO tm.app_session (id, user_id, signature, create_date, role)" +
                " VALUES (?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, session.getId());
        preparedStatement.setString(2, session.getUserId());
        preparedStatement.setString(3, session.getSignature());
        preparedStatement.setLong(4, session.getCreateDate());
        preparedStatement.setString(5, session.getRole().toString());
        @Nullable Session result = null;
        if (preparedStatement.executeUpdate() > 0) result = session;
        preparedStatement.close();
        return result;
    }

    @Override
    public void remove(@NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM tm.app_session WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Nullable
    private Session fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setUserId(row.getString("user_id"));
        session.setSignature(row.getString("signature"));
        session.setCreateDate(row.getLong("create_date"));
        session.setRole(TypeRole.valueOf(row.getString("role")));
        return session;
    }

    @Nullable
    @Override
    public Session findOne(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM tm.app_session WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Session session = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return session;
    }

    @Nullable
    @Override
    public Session merge(@NotNull final Session session) throws SQLException {
        return null;
    }

    @Override
    public void removeAll() throws SQLException {

    }

    @NotNull
    @Override
    public List<Session> findAll() throws SQLException {
        return null;
    }

    @Nullable
    @Override
    public Session remove(@NotNull final String sessionId, @NotNull final String userId) {
        return null;
    }

    @Nullable
    @Override
    public Session findOne(@NotNull final String sessionId, @NotNull final String userId) {
        return null;
    }

}
