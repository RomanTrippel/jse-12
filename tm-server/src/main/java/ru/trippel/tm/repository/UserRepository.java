package ru.trippel.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.IUserRepository;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.SortingMethod;
import ru.trippel.tm.enumeration.TypeRole;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public final class UserRepository implements IUserRepository {

    @Nullable
    public Connection connection;

    public UserRepository(@Nullable final Connection connection) {
        this.connection = connection;
    }

    @Nullable
    private User fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLoginName(row.getString("name"));
        user.setPassword(row.getString("password"));
        user.setRole(TypeRole.valueOf(row.getString("role")));
        user.setProjectSortingMethod(SortingMethod.valueOf(row.getString("project_sort")));
        user.setTaskSortingMethod(SortingMethod.valueOf(row.getString("task_sort")));
        return user;
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM tm.app_user WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final User user = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return user;
    }

    @Nullable
    @Override
    public User findByLoginName(@NotNull final String login) throws SQLException {
        @NotNull final String query = "SELECT * FROM tm.app_user WHERE name = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, login);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final User user = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() throws SQLException {
        @NotNull final String query = "SELECT * FROM tm.app_user";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<User> users = new ArrayList<>();
        while (resultSet.next()) users.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return users;
    }

    @Nullable
    @Override
    public User persist(@NotNull final User user) throws SQLException {
        @NotNull final String query = "INSERT INTO tm.app_user (id, name, password, role, project_sort, task_sort) " +
                "VALUES (?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, user.getId());
        preparedStatement.setString(2, user.getLoginName());
        preparedStatement.setString(3, user.getPassword());
        preparedStatement.setString(4, user.getRole().toString());
        preparedStatement.setString(5, user.getProjectSortingMethod().toString());
        preparedStatement.setString(6, user.getTaskSortingMethod().toString());
        @Nullable User result = null;
        if (preparedStatement.executeUpdate() > 1) result = user;
        preparedStatement.close();
        return result;
    }

    @Nullable
    @Override
    public User merge(@NotNull final User user) throws SQLException {
        @NotNull final String query = "UPDATE tm.app_user " +
                "SET name = ?, password = ?, role = ?, project_sort = ?, task_sort =? WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, user.getLoginName());
        preparedStatement.setString(2, user.getPassword());
        preparedStatement.setString(3, user.getRole().toString());
        preparedStatement.setString(4, user.getProjectSortingMethod().toString());
        preparedStatement.setString(5, user.getTaskSortingMethod().toString());
        preparedStatement.setString(6, user.getId());
        preparedStatement.executeUpdate();
        @Nullable User result = null;
        if (preparedStatement.executeUpdate() > 1) result = user;
        preparedStatement.close();
        return result;
    }

    @Override
    public void remove(@NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM tm.app_user WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String query = "DELETE FROM tm.app_user";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

}
