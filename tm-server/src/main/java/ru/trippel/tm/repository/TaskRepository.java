package ru.trippel.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.ITaskRepository;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.enumeration.Status;

import java.sql.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@NoArgsConstructor
public final class TaskRepository implements ITaskRepository {

    @Nullable
    public Connection connection;

    public TaskRepository(@Nullable final Connection connection) {
        this.connection = connection;
    }

    @Nullable
    @Override
    public Task persist(@NotNull final Task task) throws SQLException {
        @NotNull final String query = "INSERT INTO tm.app_task " +
                "(id, name, description, date_start, date_finish, project_id, user_id, status) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, task.getId());
        preparedStatement.setString(2, task.getName());
        preparedStatement.setString(3, task.getDescription());
        preparedStatement.setDate(4, new Date(task.getDateStart().getTime()));
        preparedStatement.setDate(5, new Date(task.getDateFinish().getTime()));
        preparedStatement.setString(6, task.getProjectId());
        preparedStatement.setString(7, task.getUserId());
        preparedStatement.setString(8, task.getStatus().toString());
        @Nullable Task result = null;
        if (preparedStatement.executeUpdate() > 1) result = task;
        preparedStatement.close();
        return result;
    }

    @Override
    public void clear(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "DELETE FROM tm.app_task WHERE user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String query = "DELETE FROM tm.app_task";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Nullable
    private Task fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setDateStart(row.getDate("date_start"));
        task.setDateFinish(row.getDate("date_finish"));
        task.setProjectId(row.getString("project_id"));
        task.setUserId(row.getString("user_id"));
        task.setStatus(Status.valueOf(row.getString("status")));
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll() throws SQLException {
        @NotNull final String query = "SELECT * FROM tm.app_task";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return tasks;
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM tm.app_task WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Task task = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return task;
    }

    @Nullable
    @Override
    public Task merge(@NotNull final Task task) throws SQLException {
        @NotNull final String query = "UPDATE tm.app_task SET name = ? , description = ?," +
                " date_start = ?, date_finish = ?, project_id = ?, status = ? WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, task.getName());
        preparedStatement.setString(2, task.getDescription());
        preparedStatement.setDate(3, new Date(task.getDateStart().getTime()));
        preparedStatement.setDate(4, new Date(task.getDateFinish().getTime()));
        preparedStatement.setString(5, task.getProjectId());
        preparedStatement.setString(6, task.getStatus().toString());
        preparedStatement.setString(7, task.getUserId());
        preparedStatement.setString(8, task.getId());
        @Nullable Task result = null;
        if (preparedStatement.executeUpdate() > 1) result = task;
        preparedStatement.close();
        return result;
    }

    @Override
    public void remove(@NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM tm.app_task WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId, @NotNull final Comparator<Task> comparator)
            throws SQLException {
        @NotNull final String query = "SELECT * FROM tm.app_task WHERE user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        tasks.sort(comparator);
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT * FROM tm.app_task WHERE user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return tasks;
    }

    @Override
    public void removeByProjectId(@NotNull final String projectId) throws SQLException {
        @NotNull final String query = "DELETE FROM tm.app_task WHERE project_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, projectId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @NotNull
    @Override
    public List<Task> findByPart(@NotNull final String userId, @NotNull String searchText) throws SQLException {
        @NotNull final String query = "SELECT * FROM tm.app_task " +
                "WHERE name LIKE '%" + searchText + "%' OR description LIKE '%" + searchText + "%'";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return tasks;
    }

    @Override
    public void persist(@NotNull final List<Task> taskList) throws SQLException {
        removeAll();
        for (@NotNull final Task task: taskList) {
            persist(task);
        }
    }

}