package ru.trippel.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.repository.IProjectRepository;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.enumeration.Status;

import java.sql.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@NoArgsConstructor
public final class ProjectRepository implements IProjectRepository {

    @Nullable
    public Connection connection;

    public ProjectRepository(@Nullable final Connection connection) {
        this.connection = connection;
    }

    @Nullable
    @Override
    public Project persist(@NotNull final Project project) throws SQLException {
        @NotNull final String query = "INSERT INTO tm.app_project " +
                "(id, name, description, date_start, date_finish, user_id, status) VALUES (?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, project.getId());
        preparedStatement.setString(2, project.getName());
        preparedStatement.setString(3, project.getDescription());
        preparedStatement.setDate(4, new Date(project.getDateStart().getTime()));
        preparedStatement.setDate(5, new Date(project.getDateFinish().getTime()));
        preparedStatement.setString(6, project.getUserId());
        preparedStatement.setString(7, project.getStatus().toString());
        @Nullable Project result = null;
        if (preparedStatement.executeUpdate() > 1) result = project;
        preparedStatement.close();
        return result;
    }

    @Nullable
    private Project fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setDateStart(row.getDate("date_start"));
        project.setDateFinish(row.getDate("date_finish"));
        project.setUserId(row.getString("user_id"));
        project.setStatus(Status.valueOf(row.getString("status")));
        return project;
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId, @NotNull final Comparator<Project> comparator)
            throws SQLException {
        @NotNull final String query = "SELECT * FROM tm.app_project WHERE user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) projects.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        projects.sort(comparator);
        return projects;
    }

    @Override
    public void clear(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "DELETE FROM tm.app_project WHERE user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String query = "DELETE FROM tm.app_project";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @NotNull
    @Override
    public List<Project> findAll() throws SQLException {
        @NotNull final String query = "SELECT * FROM tm.app_project";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) projects.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return projects;
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM tm.app_project WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Project project = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return project;
    }

    @Nullable
    @Override
    public Project merge(@NotNull final Project project) throws SQLException {
        @NotNull final String query = "UPDATE tm.app_project SET name = ? , description = ?," +
                " date_start = ?, date_finish = ?, status = ? WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, project.getName());
        preparedStatement.setString(2, project.getDescription());
        preparedStatement.setDate(3, new Date(project.getDateStart().getTime()));
        preparedStatement.setDate(4, new Date(project.getDateFinish().getTime()));
        preparedStatement.setString(5, project.getStatus().toString());
        preparedStatement.setString(6, project.getUserId());
        preparedStatement.setString(7, project.getId());
        @Nullable Project result = null;
        if (preparedStatement.executeUpdate() > 1) result = project;
        preparedStatement.close();
        return result;
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT * FROM tm.app_project WHERE user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) projects.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findByPart(@NotNull final String userId, @NotNull final String searchText) throws SQLException {
        @NotNull final String query = "SELECT * FROM tm.app_project " +
                "WHERE name LIKE '%" + searchText + "%' OR description LIKE '%" + searchText + "%'";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) projects.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return projects;
    }

    @Override
    public void remove(@NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM tm.app_project WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void persist(@NotNull final List<Project> projectList) throws SQLException {
        removeAll();
        for (@NotNull final Project project: projectList) {
            persist(project);
        }
    }

}
