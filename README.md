# [Task Manager](https://gitlab.com/RomanTrippel/jse-12) 
![Task Manager](https://i.ibb.co/4WZffMt/taskmanager.png)

## Software:
```
* JDK 8
* Java 1.8
* Maven 4.0
* IntelliJ IDEA
* MySQL 5.5
* Windows 10
```

## Commands to build:
```bash
mvn clean
```
```bash
mvn install
```

## Command to run:
```bash
Server:
java -jar tm-server/target/release/bin/tm-server-12.0.jar

Client:
java -jar tm-client/target/release/bin/tm-client-12.0.jar
```

### Developer:
```
Roman Trippel

rtrippel84@gmail.com
```
