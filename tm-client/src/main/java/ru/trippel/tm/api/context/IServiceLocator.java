package ru.trippel.tm.api.context;

import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.endpoint.*;
import ru.trippel.tm.api.service.IStateService;
import ru.trippel.tm.api.service.ITerminalService;

public interface IServiceLocator {

    @NotNull
    ITerminalService getTerminalService();

    @NotNull
    IServiceLocator getServiceLocator();

    @NotNull
    IStateService getStateService();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    ISessionEndpoint getSessionEndpoint();

    @NotNull
    IDataEndpoint getDataEndpoint();

}
