package ru.trippel.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Session;
import ru.trippel.tm.command.AbstractCommand;

import java.util.List;

public interface IStateService {

    @Nullable
    Session getSession();

    void setSession(@Nullable Session session);

    @NotNull
    List<AbstractCommand> getCommands();

}
