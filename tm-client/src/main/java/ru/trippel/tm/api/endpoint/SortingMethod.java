
package ru.trippel.tm.api.endpoint;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sortingMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="sortingMethod"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="CREATION_ORDER"/&gt;
 *     &lt;enumeration value="START_DATE"/&gt;
 *     &lt;enumeration value="FINISH_DATE"/&gt;
 *     &lt;enumeration value="STATUS"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "sortingMethod")
@XmlEnum
public enum SortingMethod {

    CREATION_ORDER,
    START_DATE,
    FINISH_DATE,
    STATUS;

    public String value() {
        return name();
    }

    public static SortingMethod fromValue(String v) {
        return valueOf(v);
    }

}
