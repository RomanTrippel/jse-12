package ru.trippel.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.endpoint.Task;

@NoArgsConstructor
public final class TaskPrintUtil {

    @NotNull
    public static String print(@NotNull final Task Task) {
        @NotNull final String name = Task.getName();
        @NotNull final String id = Task.getId();
        @NotNull final String description = Task.getDescription();
        @NotNull final String dateStart = Task.getDateStart().toString();
        @NotNull final String dateFinish = Task.getDateFinish().toString();
        @NotNull final String status = Task.getStatus().toString();
        return String.format("Task - %s, id - %s, description - %s, dateStart - %s, dateFinish - %s, status - %s"
                , name, id, description, dateStart, dateFinish, status);
    }

}
