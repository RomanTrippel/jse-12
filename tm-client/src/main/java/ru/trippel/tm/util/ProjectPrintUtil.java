package ru.trippel.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.endpoint.Project;

@NoArgsConstructor
public final class ProjectPrintUtil {

    @NotNull
    public static String print(@NotNull final Project project) {
        @NotNull final String name = project.getName();
        @NotNull final String id = project.getId();
        @NotNull final String description = project.getDescription();
        @NotNull final String dateStart = project.getDateStart().toString();
        @NotNull final String dateFinish = project.getDateFinish().toString();
        @NotNull final String status = project.getStatus().toString();
        return String.format("Project - %s, id - %s, description - %s, dateStart - %s, dateFinish - %s, status - %s"
                , name, id, description, dateStart, dateFinish, status);
    }

}
