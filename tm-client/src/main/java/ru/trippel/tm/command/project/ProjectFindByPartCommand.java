package ru.trippel.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Project;
import ru.trippel.tm.api.endpoint.Session;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.util.ProjectPrintUtil;

import java.util.List;

@NoArgsConstructor
public final class ProjectFindByPartCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "project find by part";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Search for a project by name or description.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        System.out.println("Enter the part of the name or description to search.");
        @NotNull final String searchText = serviceLocator.getTerminalService().read();
        @Nullable final List<Project> foundProjectList = serviceLocator.getProjectEndpoint()
                .findProjectByPart(session, searchText);
        System.out.println("List of found projects:");
        if (foundProjectList != null) {
            if (foundProjectList.size() != 0) {
                for (@NotNull final Project project : foundProjectList) {
                    System.out.println(ProjectPrintUtil.print(project));
                }
            }
        }
        System.out.println("Search for matches is over.");
    }

}
