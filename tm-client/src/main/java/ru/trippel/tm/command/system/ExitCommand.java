package ru.trippel.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.view.BootstrapView;

@NoArgsConstructor
public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "exit";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Exit of the application.";
    }

    @Override
    public void execute() {
        BootstrapView.printGoodbye();
        System.exit(0);
    }

}
