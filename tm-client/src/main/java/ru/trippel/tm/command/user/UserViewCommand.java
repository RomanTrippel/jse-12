package ru.trippel.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Exception_Exception;
import ru.trippel.tm.api.endpoint.Session;
import ru.trippel.tm.api.endpoint.TypeRole;
import ru.trippel.tm.api.endpoint.User;
import ru.trippel.tm.command.AbstractCommand;

import java.util.List;

@NoArgsConstructor
public final class UserViewCommand extends AbstractCommand {

    {
        setRole(TypeRole.ADMIN);
    }

    @NotNull
    @Override
    public String getNameCommand() {
        return "user view";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "View all user names.";
    }

    @Override
    public void execute() throws Exception_Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @NotNull final List<User> userList = serviceLocator.getUserEndpoint().findAllUser(session);
        if (userList.size() == 0) return;
        for (@NotNull final User user : userList) {
            System.out.printf("%s - %s - %s - %s - %s - %s\n"
                    , user.getId(), user.getLoginName(), user.getPassword(), user.getRole()
                    , user.getProjectSortingMethod(), user.getTaskSortingMethod());
        }
    }

}
