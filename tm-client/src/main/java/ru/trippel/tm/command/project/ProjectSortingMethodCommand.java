package ru.trippel.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Session;
import ru.trippel.tm.api.endpoint.SortingMethod;
import ru.trippel.tm.api.endpoint.User;
import ru.trippel.tm.command.AbstractCommand;

@NoArgsConstructor
public final class ProjectSortingMethodCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "project sort method";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change the sorting method.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session == null) return;
        @NotNull final User user = serviceLocator.getUserEndpoint().findOneUser(session, session.getUserId());
        @NotNull final SortingMethod[] sortingMethod = SortingMethod.values();
        for (int i = 0; i < sortingMethod.length; i++) {
            System.out.println(i+1 + ". " +sortingMethod[i].name());
        }
        System.out.println("Enter a sorting method.");
        int statusNum = -1;
        statusNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final SortingMethod newSortingMethod = sortingMethod[statusNum];
        user.setProjectSortingMethod(newSortingMethod);
        serviceLocator.getUserEndpoint().updateUser(session, user);
        System.out.println("Changes applied.");
    }

}
