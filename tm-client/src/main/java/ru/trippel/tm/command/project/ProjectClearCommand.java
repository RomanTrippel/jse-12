package ru.trippel.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Exception_Exception;
import ru.trippel.tm.api.endpoint.Session;
import ru.trippel.tm.command.AbstractCommand;

@NoArgsConstructor
public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "project clear";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception_Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        serviceLocator.getProjectEndpoint().clearProjects(session);
        System.out.println("The list of projects has been cleared.");
    }

}
