package ru.trippel.tm.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.endpoint.TypeRole;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    @NotNull
    protected TypeRole role = TypeRole.USER;

    @NotNull
    public abstract String getNameCommand();

    public abstract boolean secure ();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

}

