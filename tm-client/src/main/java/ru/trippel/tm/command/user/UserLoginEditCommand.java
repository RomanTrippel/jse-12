package ru.trippel.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Exception_Exception;
import ru.trippel.tm.api.endpoint.Session;
import ru.trippel.tm.api.endpoint.User;
import ru.trippel.tm.command.AbstractCommand;

import java.io.IOException;

@NoArgsConstructor
public final class UserLoginEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "user login edit";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit a profile.";
    }

    @Override
    public void execute() throws IOException, Exception_Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @NotNull final String userId = serviceLocator.getStateService().getSession().getUserId();
        @Nullable final User userTemp = serviceLocator.getUserEndpoint().findOneUser(session, userId);
        if (userTemp == null) {
            System.out.println("Editable user is not defined.");
            return;
        }
        System.out.println("Enter new Login.");
        @NotNull final String loginNew = serviceLocator.getTerminalService().read();
        userTemp.setLoginName(loginNew);
        serviceLocator.getUserEndpoint().updateUser(session, userTemp);
        serviceLocator.getSessionEndpoint().removeSession(session);
        serviceLocator.getStateService().setSession(null);
        System.out.println("Changes applied. Login required again.");
    }

}
