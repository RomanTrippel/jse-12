package ru.trippel.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Project;
import ru.trippel.tm.api.endpoint.Session;
import ru.trippel.tm.api.endpoint.Task;
import ru.trippel.tm.command.AbstractCommand;

import java.util.List;

@NoArgsConstructor
public final class ProjectAttachTaskCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "project attach task";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Attach a Task to a Project.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @Nullable final List<Project> projectList = serviceLocator.getProjectEndpoint()
                .findAllProjectsByUserId(session);
        if (projectList == null || projectList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        @Nullable final List<Task> taskList = serviceLocator.getTaskEndpoint().findAllTasksByUserId(session);
        if (taskList == null || taskList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        int projectNum = -1;
        int taskNum = -1;
        System.out.println("Projects List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number");
        projectNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final String projectId = projectList.get(projectNum).getId();
        System.out.println("Tasks List:");
        for (int i = 0; i < taskList.size(); i++) {
            if (taskList.get(i).getProjectId().isEmpty()) {
                System.out.println(i+1 + ". " + taskList.get(i).getName());
            }
        }
        System.out.println("Enter a task number");
        taskNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final String taskId = taskList.get(taskNum).getId();
        @Nullable final Task task = serviceLocator.getTaskEndpoint().findOneTask(session, taskId);
        if (task == null) return;
        task.setProjectId(projectId);
        serviceLocator.getTaskEndpoint().updateTask(session, task);
        System.out.println(task.getName());
        System.out.println(task.getProjectId());
        System.out.println("The task attached.");
    }

}
