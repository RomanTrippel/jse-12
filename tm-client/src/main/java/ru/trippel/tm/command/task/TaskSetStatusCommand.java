package ru.trippel.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Task;
import ru.trippel.tm.api.endpoint.Session;
import ru.trippel.tm.api.endpoint.Status;
import ru.trippel.tm.command.AbstractCommand;

import java.util.List;

@NoArgsConstructor
public final class TaskSetStatusCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "task set status";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change of task status.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @Nullable final List<Task> taskList = serviceLocator.getTaskEndpoint().findAllTasksByUserId(session);
        if (taskList == null || taskList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        int taskNum = -1;
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            System.out.println(i+1 + ". " + taskList.get(i).getName());
        }
        System.out.println("Enter a task number to change status.");
        taskNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final String taskId = taskList.get(taskNum).getId();
        @Nullable final Task task = serviceLocator.getTaskEndpoint().findOneTask(session, taskId);
        if (task == null) return;
        @NotNull final Status[] status = Status.values();
        for (int i = 0; i < status.length; i++) {
            System.out.println(i+1 + ". " +status[i]);
        }
        System.out.println("Enter a status.");
        int statusNum = -1;
        statusNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final Status newStatus = status[statusNum];
        task.setStatus(newStatus);
        serviceLocator.getTaskEndpoint().updateTask(session,task);
        System.out.println("Changes applied.");
    }

}
