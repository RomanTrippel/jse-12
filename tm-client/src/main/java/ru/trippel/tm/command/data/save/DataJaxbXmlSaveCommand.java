package ru.trippel.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Exception_Exception;
import ru.trippel.tm.api.endpoint.JAXBException_Exception;
import ru.trippel.tm.api.endpoint.Session;
import ru.trippel.tm.api.endpoint.TypeRole;
import ru.trippel.tm.command.AbstractCommand;

@NoArgsConstructor
public final class DataJaxbXmlSaveCommand extends AbstractCommand {

    {
        setRole(TypeRole.ADMIN);
    }

    @NotNull
    @Override
    public String getNameCommand() {
        return "data jaxb xml save";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Saving data as XML using JAXB.";
    }

    @Override
    public void execute() throws JAXBException_Exception, Exception_Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        serviceLocator.getDataEndpoint().dataJaxbXmlSave(session);
        System.out.println("Data saved.");
    }

}
