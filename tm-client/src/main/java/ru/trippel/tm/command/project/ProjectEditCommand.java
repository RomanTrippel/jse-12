package ru.trippel.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Exception_Exception;
import ru.trippel.tm.api.endpoint.Project;
import ru.trippel.tm.api.endpoint.Session;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.util.DateUtil;

import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public final class ProjectEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "project edit";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit a project.";
    }

    @Override
    public void execute() throws IOException, NumberFormatException, Exception_Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @Nullable final List<Project> projectList = serviceLocator.getProjectEndpoint().findAllProjectsByUserId(session);
        if (projectList == null || projectList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        int projectNum = -1;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number to Edit.");
        projectNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final String projectId = projectList.get(projectNum).getId();
        @Nullable final Project project = serviceLocator.getProjectEndpoint().findOneProject(session, projectId);
        if (project == null) return;
        System.out.println("If you don't want to change, leave the line empty.");
        System.out.println("Enter new Name.");
        @NotNull final String newName = serviceLocator.getTerminalService().read();
        if (!newName.isEmpty()) project.setName(newName);
        System.out.println("Enter description.");
        @NotNull final String newDescription = serviceLocator.getTerminalService().read();
        if (!newDescription.isEmpty()) project.setDescription(newDescription);
        System.out.println("Enter the start date of the project.(For example, 13-01-2020)");
        @NotNull final String newDateStart = serviceLocator.getTerminalService().read();
        if (!newDateStart.isEmpty()){
            @NotNull final XMLGregorianCalendar dateStart = DateUtil.parseDate(newDateStart);
            project.setDateStart(dateStart);
        }
        System.out.println("Enter the finish date of the project.(For example, 13-01-2020)");
        @NotNull final String newDateFinish = serviceLocator.getTerminalService().read();
        if (!newDateFinish.isEmpty()){
            @NotNull final XMLGregorianCalendar dateFinish = DateUtil.parseDate(newDateFinish);
            project.setDateFinish(dateFinish);
        }
        serviceLocator.getProjectEndpoint().updateProject(session,project);
        System.out.println("Changes applied.");
    }

}
