package ru.trippel.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trippel.tm.api.endpoint.Project;
import ru.trippel.tm.api.endpoint.Session;
import ru.trippel.tm.api.endpoint.Status;
import ru.trippel.tm.command.AbstractCommand;

import java.util.List;

@NoArgsConstructor
public final class ProjectSetStatusCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getNameCommand() {
        return "project set status";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change of project status.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @Nullable final List<Project> projectList = serviceLocator.getProjectEndpoint().findAllProjectsByUserId(session);
        if (projectList == null || projectList.isEmpty()) {
            System.out.println("List is empty.");
            return;
        }
        int projectNum = -1;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number to change status.");
        projectNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final String projectId = projectList.get(projectNum).getId();
        @Nullable final Project project = serviceLocator.getProjectEndpoint().findOneProject(session, projectId);
        if (project == null) return;
        @NotNull final Status[] status = Status.values();
        for (int i = 0; i < status.length; i++) {
            System.out.println(i+1 + ". " +status[i]);
        }
        System.out.println("Enter a status.");
        int statusNum = -1;
        statusNum +=  Integer.parseInt(serviceLocator.getTerminalService().read());
        @NotNull final Status newStatus = status[statusNum];
        project.setStatus(newStatus);
        serviceLocator.getProjectEndpoint().updateProject(session,project);
        System.out.println("Changes applied.");
    }

}
